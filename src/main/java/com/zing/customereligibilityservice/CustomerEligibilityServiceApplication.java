package com.zing.customereligibilityservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerEligibilityServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerEligibilityServiceApplication.class, args);
	}

}
