package com.zing.customereligibilityservice.dto;

public enum PaymentType {
    DEBIT,CREDIT;
}
