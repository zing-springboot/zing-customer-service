package com.zing.customereligibilityservice.dto;

public enum Category {
    GROCERIES,HEALTH,ENTERTAINMENT,TRAVEL,FOOD,CLOTHS;
}
