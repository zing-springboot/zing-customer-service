package com.zing.customereligibilityservice.exception;

import lombok.Data;

@Data
public class CustomCustomerEligibilityException extends RuntimeException{


    private String errorCode;

    public CustomCustomerEligibilityException(String message, String errorCode)
    {
        super(message);
        this.errorCode = errorCode;
    }

}
