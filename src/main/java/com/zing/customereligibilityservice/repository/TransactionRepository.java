package com.zing.customereligibilityservice.repository;

import com.zing.customereligibilityservice.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction,Long> {
}
