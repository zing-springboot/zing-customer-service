package com.zing.customereligibilityservice.repository;

import com.zing.customereligibilityservice.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository  extends JpaRepository<Customer,Long> {
}
